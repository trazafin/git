# Résumé des commandes GIT
---

En complément des vidéos:
- Introduction générale: https://www.youtube.com/watch?v=4b7RJLuZ25s
- Commandes de bases: https://www.youtube.com/watch?v=ZvGIJeFP7xA


Les commandes de bases:
- `git log` : permet de voir l'etat de vos commit (l'arbre de vos commit). C'est l'historique de vos commit
- `git status` : permet de voir l'état de votre répertoire de travail. Cette commande indique les modifications, ce qu'il faut commiter, ce qu'il faut ajouter à l'index et/ou ce qu'il faut envoyer à votre dépôt distant (gitlab)
- `git add <fff>` : permet d'ajouter à votre index les modifications, les fichiers et/ou les repertoire ici fff.
- `git commit -m "message de description du commit"` : permet de commiter (sauvegarder, mettre dans votre historique, versionner) ce qui se trouve dans l'index.
- `git push origin master` : permet d'envoyer vers votre dépôt distant les commits (il est possible d'envoyer plusieurs commit en une seule fois)

Les commandes de branches:
- `git branch` : permet de lister les branches de votre dépôt et de voir dans quelle branche vous êtes actuellement
- `git branch test`: permet de créer la branche `test`
- `git checkout test` : permet de vous positionner sur la branche `test`. Vous pouvez y faire les modifications que vous voulez, mais avant de quitter cette branche vous devez faire `git add, git commit`
- `git checkout master` : vous permet de vous positionner sur la branch principale dont le nom par défaut est master
- `git merge test` : si vous êtes sur une branche autre que `test` cette commande permets d'intégrer les modifications présentes sur la branche test dans la branche actuelle par exemple master

Les commandes de retour en arrière:
- Si vous avez modifier un fichier mais que vous ne l'avez pas encore ajoute à l'index: `git checkout -- <fichier>` vous permets de revenir à la version précédente du fichier
- Si vous avez déjà fait un `git add` de votre fichier (celui-ci est déjà dans l'index`: `git reset HEAD <fichier>` permet de retirer le fichier de l'index
- Si vous avez dejà fait un `git add` et un `git commit` de votre fichier (votre fichier est commiter) : `git revert HEAD~1..HEAD` permet d'annuler le dernier commit. Pour annuler le dernier commit, git créer un nouveau commit. cette action permet de voir précisement l'évolution de votre dépôt et de ne perdre aucune action effectué sur votre dépôt.

## Méthode de travail
---

Supposons que và partir de maintenant vous décidiez de mettre tout votre code, et vos
fichiers textes sous git pour le module M12XX. Les grandes étapes à suivre sont 
suivantes. Je suppose ici que vous avez déja crée votre compte gitlab.

1. Création du dépot git sur gitlab
---
Connetez-vous sur gitlab.com avec votre compte et créer un dépôt (public ou privé)
du nom de M12XX pour être cohérent.

Un fois le dépôt créer, créer le fichier README.md (en ligne) et mettez-y une
description rapide du contenu de votre dépôt. Par exemple: Ce dépot va contenir
les notes de cours, résultats de TD et TP du module M12XX.

Retenez bien l'adresse de votre dépôt qui doit être: https://gitlab.com/VotreLogin/M12XX.git
(bien sur VotreLogin doit être remplacé par votre login)

2. Copie de votre dépôt se trouvant sur Gitlab vers votre machine locale
---
Même si vous utilisez votre propre machine et pas celles du département, je vous recommande
fortement d'utiliser gitlab.

Ouvrez un terminal et positionner vous dans le répertoire dans lequel vous voulez
mettre votre travail. par exemple: `cd cours`

Copier votre dépôt (celui qui se trouve sur gitlab) vers votre machine local:
```
git clone https://gitlab.com/VotreLogin/M12XX.git
```
Cette commande va créer un répertoire `M12XX` dans votre répertoire. 
Positionnez vous dans ce répertoire avec la commande : `cd cours/M12XX`

Dans ce répertoire, vous devriez avoir un fichier `README.md` celui crée à l'étape
précédente. 

3. Utilisation de votre dépôt
--- 

Maintenant vous pouvez commencer à travailler de manière classique. Par exemple 
en lançant votre éditeur de texte favoris, en utilisant matlab, etc...

Vous devez sauvegarder vos fichiers dans le repertoire `cours/M12XX`. A chaque 
exercice, ou à chaque fois fois que vous avez du code, du texte dont vous êtes satisfait
envoyer le sur votre dépôt. 

Pour le faire. Supposons que vous avez créer un fichier `tp1exo1.c` dans votre répertoire
`cours/M12XX`. Positionnez vous dans `cd cours/M12XX` 

Suivez les étapes suivantes: __Attention il faut les faire dans cet ordre__

```
git add tp1exo1.c
```

Cette commande vous permet d'ajouter le fichier `tp1exo1.c` à l'index de votre dépôt.

```
git commit -m "Solution de l'exercice 1 du tp1"
```

Cette commande vous permet de "commiter" vos modifications. Le message doit bien sur refleter
le contenu de votre fichier ou les modifications.

```
git push origin master
```

Cette commande vous permet d'envoyer vos modifications sur gitlab.com.
À la fin du tp, vous pouvez supprimer le répertoire `cours/M12XX` si vous êtes sur une machine du
département, vos fichiers sont déjà (sauvegardés) sur gitlab.com.

4. Ré-utilisation de votre dépôt
--- 

Au TP ou TD suivant, vous pouvez retouver tous vos fichiers. voici la procédure.

- Sur votre machine ou celle du département, créer un répertoire par exemple `mkdir cours`
- Positionnez vous dans ce répertoire `cd cours`
- Copiez votre dépôt depuis gitlab vers la machine: `git clone https://gitlab.com/VotreLogin/M12XX.git`
- La commande précédente créer le répertoire `M12XX` positionnez vous dans ce répertoire: `cd M12XX`
- Si vous modifier le fichier `tp1exo1.c` et que vous créer un fichier `tp2exo2.c` vous devez faire
    - `git add tp1exo1.c tp2exo2.c`
    - `git commit -m "modification de tp1exo1 et ajout de tp2exo2"`
    - `git push origin master`
Voila, vos modifications sont de nouveau sur gitlab.com. Chaque fichiers modifiés ou nouveau fichier que
vous créer doit passer par les 3 étapes : `git add`, `git commit`, `git push`. Vérifier toujours que tous les
fichiers sont bien présents sur votre dépôt gitlab.com

5. Notes
---

- Cette méthode de travail fonctionne avec les répertoires. Si vous créer un répertoire `html` dans
`cours/M12XX`. Vous pouvez faire un `git add html/`. Il faut retenir qu'un répertoire est juste un fichier 
particulier. Bien sur ensuite vous faites `git commit -m "ajout d'un repertoire"`, et `git push origin master`
- L'utilisation de git est recommandé pour tous les fichiers textes. Langage C, ou autre langage de programmation,
matlab, html, css, etc... 
- Je vous conseille de faire un `git add`, `git commit`, `git push` le plus souvent possible (3 ou 4 fois par exercice, 
ou toutes les 5 lignes écrites). Car git vous permet aussi de garder un historique de votre travail et vous permet facilement de revenir en arrière.
